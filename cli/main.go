package main

import (
	"database/sql"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"

	argparse "github.com/akamensky/argparse"
	sqlite3 "github.com/mattn/go-sqlite3"
)

type record struct {
	id   int64
	path string
	blob []byte
}

/*
Vulunerbility: SQL injection is possible.
*/

func main() {
	parser := argparse.NewParser(
		"path2blob",
		"A simple tool for spatialite/GPKG file database, read file path from text column and write back as blob.")

	idColumn := parser.String(
		"i",
		"id",
		&argparse.Options{
			Required: true,
			Help:     "Primary key / Unique ID column's name.",
		})

	pathColumn := parser.String(
		"p",
		"path",
		&argparse.Options{
			Required: true,
			Help:     "Column contains path to file.",
		})

	files := parser.StringList(
		"f",
		"files",
		&argparse.Options{
			Required: true,
			/*
				Validate: func(files []string) error {
					for _, file := range files {
						fmt.Println(file)
					}

					if len(args) < 2 {
						return errors.New("both input and output file should be provided")
					}

					if len(args) > 2 {
						return errors.New("only one input and output file should be provided")
					}
					return nil
				},
			*/
			// Validate can only act on each element of the list, not the list itself
			Help: "First is input file, last is output file.",
		})

	layerName := parser.String(
		"l",
		"layer",
		&argparse.Options{
			Required: true,
			Help:     "Layer to operate on",
		})

	err := parser.Parse(os.Args)

	if err != nil {
		// In case of error print error and print usage
		// This can also be done by passing -h or --help flags
		fmt.Print(parser.Usage(err))
	}

	if len(*files) < 2 {
		err = errors.New("both input and output file should be provided")

	}

	if len(*files) > 2 {
		err = errors.New("only one input and one output file should be provided, space not escaped?")

	}

	if err != nil {
		log.Fatal(err)
	}

	//TODO: file readable / writable
	//fmt.Println(*idColumn, *pathColumn, *files, *layerName)

	srcFile, err := os.Open((*files)[0])
	if err != nil {
		log.Fatal(err)
	}
	//defer srcFile.Close()

	dstFile, err := os.Create((*files)[1])
	if err != nil {
		log.Fatal(err)
	}
	//defer dstFile.Close()

	_, err = io.Copy(dstFile, srcFile)

	if err != nil {
		log.Fatal(err)
	}

	// https://github.com/shaxbee/go-spatialite/blob/9b4c81899e0e/spatialite.go MIT (C) shaxbee
	type entrypoint struct {
		lib  string
		proc string
	}

	var LibNames = []entrypoint{
		{"mod_spatialite", "sqlite3_modspatialite_init"},
		{"mod_spatialite.dylib", "sqlite3_modspatialite_init"},
		{"libspatialite.so", "sqlite3_modspatialite_init"},
		{"libspatialite.so.5", "spatialite_init_ex"},
		{"libspatialite.so", "spatialite_init_ex"},
	}
	var ErrSpatialiteNotFound = errors.New("shaxbee/go-spatialite: spatialite extension not found")

	sql.Register("spatialite", &sqlite3.SQLiteDriver{
		ConnectHook: func(conn *sqlite3.SQLiteConn) error {
			for _, v := range LibNames {
				if err := conn.LoadExtension(v.lib, v.proc); err == nil {
					return nil
				}
			}
			return ErrSpatialiteNotFound
		},
	})
	//

	db, err := sql.Open("spatialite", (*files)[1])
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}

	countStmt := fmt.Sprintf("SELECT COUNT(%s) FROM %s WHERE %s IS NOT NULL;", *pathColumn, *layerName, *pathColumn)
	log.Println(countStmt)
	countRows, err := db.Query(countStmt)
	if err != nil {
		log.Fatal(err)
	}

	var count int64

	countRows.Next()
	err = countRows.Scan(&count)
	countRows.Close()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Records to update: %d", count)

	queryStmt := fmt.Sprintf("SELECT %s,%s FROM %s WHERE %s IS NOT NULL;", *idColumn, *pathColumn, *layerName, *pathColumn)
	rows, err := db.Query(queryStmt)

	if err != nil {
		log.Fatal(err)
	}

	var records []record
	for rows.Next() {
		var tmpRecord record
		err = rows.Scan(&(tmpRecord.id), &(tmpRecord.path))
		if err != nil {
			log.Fatal(err)
		}

		fileDir := filepath.Dir((*files)[0])
		path := fmt.Sprintf("%s/%s", fileDir, tmpRecord.path)

		tmpRecord.blob, err = os.ReadFile(path)
		if err != nil {
			log.Fatal(err)
		}
		records = append(records, tmpRecord)

	}

	rows.Close()

	altDelStmt := fmt.Sprintf("ALTER TABLE %s DROP COLUMN %s;", *layerName, *pathColumn)
	_, err = db.Exec(altDelStmt)
	if err != nil {
		log.Fatal(err)
	}

	altCreatStmt := fmt.Sprintf("ALTER TABLE %s ADD COLUMN %s BLOB;", *layerName, *pathColumn)
	_, err = db.Exec(altCreatStmt)
	if err != nil {
		log.Fatal(err)
	}

	updStmtStr := fmt.Sprintf("UPDATE %s SET %s = ? WHERE %s.%s = ? ;", *layerName, *pathColumn, *layerName, *idColumn)
	updStmt, err := db.Prepare(updStmtStr)
	if err != nil {
		log.Fatal(err)
	}

	for _, r := range records {
		updStmt.Exec(r.blob, r.id)
		if err != nil {
			log.Fatal(err)
		}
	}

}
