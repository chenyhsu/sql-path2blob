APPNAME=path2blob
PWD=`pwd`
deps: 
	# Download required modules
	export GO111MODULE="on"
	go mod download 
	echo ${PWD}

build: deps
	# Build binary 
	export GO111MODULE="on"
	go build -o ./build/${APPNAME} ./cli

.PHONY: install
install: build
	# Install built binary 
	sudo cp ./build/${APPNAME} /usr/bin/${APPNAME}
	sudo chmod a+x /usr/bin/${APPNAME}



.PHONY: clean
clean: 
	# Clean build files
	export GO111MODULE="on"
	go clean
	rm -rf ./build

.PHONY: uninstall
uninstall: 
	sudo rm -rf /usr/bin/${APPNAME}
